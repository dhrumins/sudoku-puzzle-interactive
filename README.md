This is the program that allows the user to solve a particular sudoku puzzle.
This is the first version of it and user can solve only one particular puzzle, that the program provides. It is an interactive
game that asks user where he wants to put a number of his choice and at every stage, it checks if the number placed on the grid
at a particular position, conflicts with the other numbers in the same row, or same column or same 3X3 matrix that it belongs to.

Some other features are missing, like randomizing the sudoku puzzle generator and giving out a new puzzle on every run,
heuristic to figure out the possible future conflicts as user puts various numbers on the grid and ending the puzzle on that
occasion, etc. 