package sudoku;

import java.util.Scanner;

import javax.naming.directory.InvalidAttributeValueException;

public class SudokuGame {

	private static final int[][] SUDOKU = new int[][]{{3,0,4,2,7,5,8,0,0},{0,7,0,8,0,0,0,0,3},{8,0,1,4,0,0,0,7,0},
        {0,2,0,6,0,8,0,0,0},{0,5,7,0,0,0,6,3,0},{0,0,0,7,0,4,0,5,0},
        {0,4,0,0,0,1,3,0,5},{9,0,0,0,0,7,0,4,0},{0,0,2,3,4,6,9,0,7}};

	public static void main(String[] args) throws InvalidAttributeValueException {
		
		System.out.println("Welcome to SUDOKU puzzle!!\n");
		display();
		playGame();
	}

	private static void playGame() throws InvalidAttributeValueException {

		boolean cannotSolve = false;
		Scanner scan = new Scanner(System.in);
		while (true) {

			if (!isAnyCellEmpty()) {
				System.out.println("\nSudoku puzzle solved!!\n");
				break;
			}

			System.out.println("\n\nWhere would you like to place your number?");
			System.out.print("Row: ");
			int displayRow = scan.nextInt();
			int row = displayRow - 1;
			System.out.print("Column: ");
			int displayColumn = scan.nextInt();
			int column = displayColumn - 1;

			if (displayRow == -1 || displayColumn == -1) {
				cannotSolve = true;
				break;
			}

			if (row < 0 || row > 8 || column < 0 || column > 8) {
				System.out.println("Invalid row or column. Both must be between (1-9)");
				continue;
			}

			if (SUDOKU[row][column] != 0) {
				System.out.println("\nRow " + displayRow + " and column " + displayColumn + " is already occupied with number " + SUDOKU[row][column] + " ...\n");
				continue;
			}

			while (true) {
				System.out.print("\n\nEnter a number for Row: " + displayRow + " and Column: " + displayColumn + " (Enter -1 to forfeit the puzzle) :");
				int number = scan.nextInt();

				if (number == -1) {
					cannotSolve = true;
					break;
				}

				if (number < 1 || number > 9) {
					System.out.println("Invalid number....Must be between (1-9)\n");
					continue;
				}

				// check whether number is conflicting or not
				if (isConflicting(row, column, number)) {
					continue;
				} else {
					SUDOKU[row][column] = number;
					break;
				}
			}

			if (cannotSolve) {
				break;
			}

			System.out.println("Putting number " + SUDOKU[row][column] + " at row = " + displayRow + " and column = " + displayColumn + "\n");
			display();
			System.out.println("\n");
		}

		if (cannotSolve) {
			display();
			System.out.println("\n\nWell Tried....But could not solve the Sudoku puzzle...!!\n\n");
		}
	}

	private static boolean isAnyCellEmpty() {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (SUDOKU[i][j] == 0) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean isConflicting(int row, int column, int number) {
		for (int i = 0; i < 9; i++) {
			if (SUDOKU[i][column] == number) {
				System.out.println("Number " + number + " conflicts with Row = " + (i + 1) + " and Column = " + (column + 1));
				return true;
			}
		}

		for (int j = 0; j < 9; j++) {
			if (SUDOKU[row][j] == number) {
				System.out.println("Number " + number + " conflicts with Row = " + (row + 1) + " and Column = " + (j + 1));
				return true;
			}
		}

		int matrixRow = row - (row % 3);
		int matrixColumn = column - (column % 3);

		for (int i = matrixRow; i < (matrixRow + 3); i++) {
			for (int j = matrixColumn; j < (matrixColumn + 3); j++) {
				if (SUDOKU[i][j] == number) {
					System.out.println("Number " + number + " conflicts with Row = " + (i + 1) + " and Column = " + (j + 1));
					return true;
				}
			}
		}
		return false;
	}

	private static void display() {
		for (int i = 0; i < SUDOKU.length; i++) {
			if (i != 0 && i % 3 == 0) {
				displayHorizantalLine();
			}

			for (int j = 0; j < SUDOKU[0].length; j++) {
				if (j % 3 == 0) {
					System.out.print("| ");
				}
				System.out.print((SUDOKU[i][j] == 0 ? "*" : SUDOKU[i][j]) + " ");
			}
			System.out.print("| ");
			System.out.println("");
		}
	}

	private static void displayHorizantalLine() {
		for (int i = 0; i < 13; i++) {
			System.out.print("- ");
		}

		System.out.println("");
	}
}
